# 656project

Project including server code, client program, dataset files.

1. Server builing code:

run source ./build.sql to create all the tables for **selected** database.

run source ./clean.sql to drop all the tables created by build.sql.

Does not contain creating and selecting database

Upon testing, all the csv files should be in path /tmp/debug or change the import dataset path in each building .sql file.

2. Client code:

Run python 656client and follow the instructions.
