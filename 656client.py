#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pymysql.cursors
import os


# In[ ]:


def print_usage():
    print("""|*****************************************************************************************|""")
    print("""|*****************************************************************************************|""")
    print("""|*****************Welcome to use our Used Cars Infomation Database************************|""")
    print("""|*****************************************************************************************|""")
    print("""|Please follow our usage options of this Database client:                                 |""")
    print("""|input command U to show client usage info again                                          |""")
    print("""|input command EXIT to exit our database                                                  |""")
    # print("""|by defalut 20 data records will pop up, use  L to set                                    |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    C: get basic info of cars, without options specified will only show 20 cars          |""")
    print("""|SYNOPSIS                                                                                 |""")
    print("""|    C [OPTION]: use options to select the specific data you need                         |""")
    print("""|options: -p, -p 3000 means the car with price lower than 3000                            |""")
    print("""|options: -o, -o 4 means the car with ownercount smaller than 4                           |""")
    print("""|options: -b, -b body_type will select cars of this specified body type                   |""")
    print("""|options: -y, -y 2012 will use the date of manufacture to select data                     |""")
    print("""|options: -s, -s 5 will show cars with selling rate of 5, rate range from 0-5             |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    B: will show the distinct car body types we have on sale                             |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    S: S sp_id will show the dealer info of the specified sp_id                          |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    W: W car_id will show website info of the specified car_id                           |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    F: F car_id will show fuel info of the specified car_id                              |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    D: D car_id will show dynamic info of the specified car_id                           |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    A: A car_id will show accident and damage info of the specified car_id               |""")
    print("""|*****************************************************************************************|""")
    print("""|COMMAND NAME                                                                             |""")
    print("""|    R: R car_id will show room and space info of the specified car_id                    |""")
    print("""|*****************************************************************************************|""")
    print("""|*****************************************************************************************|""")

# print_usage()


# In[ ]:


def connect():
    connection = pymysql.connect(host='localhost',
                             user='root',
                             password='hezHNUuw1',
                             database='db656',
                             cursorclass=pymysql.cursors.DictCursor)
    return connection


# In[ ]:


# used for command B
def body_type():
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = "SELECT DISTINCT body_type FROM `car`"
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)        
# body_type()


# In[ ]:


# used for command S
def dealer_info(sp_id):
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = """SELECT sp_name, dealer_zip, franchise_dealer, city FROM `Dealer`
                    where sp_id = %s """%(sp_id)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)
# dealer_info(351457.0)


# In[ ]:


# used for command W
def web_info(sales_id):
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = """SELECT listing_color, listed_date, main_picture_url, is_new, daysonmarket 
                    FROM `SalesWebInfo`
                    where Sales_id = %s """%(sales_id)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)
# web_info(3)


# In[ ]:


# used for command F
def fuel_info(fuel_id):
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = """SELECT city_fuel_economy,highway_fuel_economy,fuel_tank_volume,fuel_type 
                    FROM `FuelInfo`
                    where fuel_id = %s """%(fuel_id)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)
# fuel_info(3)


# In[ ]:


# used for command D
def dynamic_info(car_id):
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = """SELECT engine_type,engine_cylinders,engine_displacement,horsepower,
                    torque,transmission,transmission_display,wheel_system,
                    wheel_system_display,wheelbase,power 
                    FROM `DynamicInfo`
                    where Dy_id = %s """%(car_id)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)
# dynamic_info(3)


# In[ ]:


# used for command A
def accident_info(car_id):
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = """SELECT frame_damage,has_accident,salvage,isCab,theft_title 
                    FROM `DepreciationInfo`
                    where Dep_id = %s """%(car_id)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)
# accident_info(3)


# In[ ]:


# used for command R
def Room_info(car_id):
    connection = connect()
    with connection:
        with connection.cursor() as cursor:
            sql = """SELECT back_legroom,front_legroom,height,length,width,maximum_seating 
                    FROM `SpaceInfo`
                    where Space_id = %s """%(car_id)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            print(i)
# Room_info(3)


# In[ ]:


# test = "C -p 3000 -o 4 -b Sedan -y 2018 -s 4"
# options = test.split(" ")[1:]
# options


# In[ ]:


# for i in enumerate(options):
#     print(i)


# In[ ]:


# for i in enumerate(options):
#     if i[1] == "-p":
#         price = options[i[0]+1]
#         print(price)
#     if i[1] == "-o":
#         owner_count = options[i[0]+1]
#         print(owner_count)
#     if i[1] == "-b":
#         body_type = options[i[0]+1]
#         print(body_type)
#     if i[1] == "-y":
#         year = options[i[0]+1]
#         print(year)
#     if i[1] == "-s":
#         selling_rate = options[i[0]+1]
#         print(selling_rate)


# In[189]:


# used for command C
def car_info(options):
    clause_li = []
    for i in enumerate(options):
        clause = ""
        if i[1] == "-p":
            price = options[i[0]+1]
            clause = clause + "price <= " + str(price)
            clause_li.append(clause)
        if i[1] == "-o":
            owner_count = options[i[0]+1]
            clause = clause + "owner_count = " + str(owner_count)
            clause_li.append(clause)
        if i[1] == "-b":
            body_type = options[i[0]+1]
            clause = clause + "body_type = '" + str(body_type)+"'"
            clause_li.append(clause)
        if i[1] == "-y":
            year = options[i[0]+1]
            clause = clause + "year = " + str(year)
            clause_li.append(clause)
        if i[1] == "-s":
            selling_rate = options[i[0]+1]
            clause = clause + "seller_rating = " + str(selling_rate)
            clause_li.append(clause)
    final_clause = ""
    if len(clause_li) > 1:
        for i in clause_li[:-2]:
            final_clause = final_clause + i + " and "
        final_clause += clause_li[-1]
    elif len(clause_li) == 1:
        final_clause += clause_li[0]
#     print(final_clause)
    sql = """SELECT Car_id,sp_id,make_name,model_name,price,year,milage,
                        body_type,exterior_color,interior_color,seller_rating,owner_count 
                        FROM `Car`
                        where %s """%(final_clause)        
        
    if len(clause_li) == 0:
        sql = """SELECT Car_id,sp_id,make_name,model_name,price,year,milage,
                body_type,exterior_color,interior_color,seller_rating,owner_count 
                FROM `Car` limit 20"""
    
    connection = connect()
    with connection:
        with connection.cursor() as cursor:            
#             print(sql)
            cursor.execute(sql)
            result = cursor.fetchall()
        if not result:
            print("empty set")
        for i in result:
            # print("""|*****************************************************************************************|""")
            print(i)
            # print("""|*****************************************************************************************|""")

# test = "C -p 3000 -o 4 -b Sedan -y 2018 -s 4"
# options = test.split(" ")[1:]
# options

# car_info(options)


# In[188]:


print_usage() 
while(True):    
    commandline = input("please input your command and options:")
    command = commandline.split(" ")[0]
    if command == "EXIT":
        print("*******************Bye, have a good day***************************")
        break
    if command == "U":
        print_usage()
    if command == "C":
        options = commandline.split(" ")[1:]
        car_info(options)
    if command == "B":
        body_type()
    if command == "S":
        sp_id = commandline.split(" ")[1]
        dealer_info(sp_id)
    if command == "W":
        sales_id = commandline.split(" ")[1]
        web_info(sales_id)
    if command == "F":
        fuel_id = commandline.split(" ")[1]
        fuel_info(fuel_id)
    if command == "D":
        dy_id = commandline.split(" ")[1]
        dynamic_info(dy_id)
    if command == "A":
        car_id = commandline.split(" ")[1]
        accident_info(car_id)
    if command == "R":
        car_id = commandline.split(" ")[1]
        Room_info(car_id)

