SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS SpaceInfo;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create SpaceInfo' as '';

create table if not exists SpaceInfo (
			Space_id int NOT NULL primary key,
			-- spaceinfo_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			-- vin varchar(22),
			back_legroom varchar(7),
			front_legroom varchar(7),
			height varchar(8),
			length varchar(8),
			width varchar(8),
			maximum_seating varchar(8),

			-- PRIMARY KEY (spaceinfo_id),
			FOREIGN KEY (Space_id) REFERENCES Car(Car_id)
		  );

load data infile '/tmp/debug/SpaceInfo.csv' ignore into table SpaceInfo
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
    -- (   
	-- 	vin,
	-- 	back_legroom,
	-- 	front_legroom,
	-- 	height,
	-- 	length,
	-- 	width,
	-- 	maximum_seating
	-- 	);