SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS SalesWebInfo;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create SalesWebInfo' as '';

create table if not exists SalesWebInfo (
			Sales_id int NOT NULL primary key,
			listing_id int,
			-- listing_id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			listing_color varchar(7),
			listed_date varchar(10),
			main_picture_url varchar(365),
			major_options varchar(1227),
			is_new boolean,
			daysonmarket int,
			FOREIGN KEY (Sales_id) REFERENCES Car(Car_id)
		  );

load data infile '/tmp/debug/SalesWebInfo.csv' ignore into table SalesWebInfo
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
    (Sales_id, listing_id, listing_color, listed_date, main_picture_url, major_options, @is_new, daysonmarket)
	set
	is_new := if(@in_new = 'True', 1, 0);


