SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS DepreciationInfo;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create DepreciationInfo' as '';

create table if not exists DepreciationInfo (
			Dep_id int not null primary key,
			-- depreciationinfo_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			-- vin varchar(22) not null primary key,
			frame_damage boolean,
			has_accident boolean,
			salvage boolean,
			isCab boolean,
			theft_title boolean,

			-- PRIMARY KEY (depreciationinfo_id),
			FOREIGN KEY (Dep_id) REFERENCES Car(Car_id)
		  );

load data infile '/tmp/debug/DepreciationInfo.csv' ignore into table DepreciationInfo
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
    (Dep_id, @frame_damage, @has_accident, @salvage, @isCab, @theft_title)
	set
	frame_damage := if(@frame_damage = 'true', 1, 0),
	has_accident := if(@has_accident = 'true', 1, 0),
	salvage := if(@salvage = 'true', 1, 0),
	isCab := if(@isCab = 'true', 1, 0),
	theft_title := if(@theft_title = 'true', 1, 0)
	;