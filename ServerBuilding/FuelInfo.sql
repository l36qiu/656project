SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS FuelInfo;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create FuelInfo' as '';

create table if not exists FuelInfo (
			Fuel_id int not null primary key,
			-- fuelinfo_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			-- vin varchar(22),
			city_fuel_economy float,
			highway_fuel_economy float,
			fuel_tank_volume varchar(8),
			fuel_type varchar(22),

			-- PRIMARY KEY (fuelinfo_id),
			FOREIGN KEY (Fuel_id) REFERENCES Car(Car_id)
		  );

load data infile '/tmp/debug/FuelInfo.csv' ignore into table FuelInfo
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
    -- (   
	-- 	vin,
	-- 	city_fuel_economy,
	-- 	highway_fuel_economy,
	-- 	fuel_tank_volume,
	-- 	fuel_type
	-- 	);