SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Dealer;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create Dealer' as '';

create table if not exists Dealer (
			-- id int NOT NULL primary key,
			sp_id float NOT NULL primary key,
			sp_name varchar(90),
			longitude float,
			latitude float,
			dealer_zip varchar(20),
			franchise_dealer boolean,
			city varchar(40)
		  );

load data infile '/tmp/debug/Dealer.csv' ignore into table Dealer
-- load data infile '/tmp/Dealer.csv' ignore into table Dealer
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
	(sp_id,sp_name,longitude,latitude,dealer_zip,@c7,city)
	set 
	franchise_dealer := if(@c7 = 'true', 1, 0)
	;
