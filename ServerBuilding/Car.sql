SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Car;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create Car' as '';

create table if not exists Car (
			Car_id int NOT NULL primary key,
			vin varchar(22),
			sp_id float,
			make_name varchar(30),
			model_name varchar(40),
			price float,
			year int,
			milage float,
			body_type varchar(20),
			exterior_color varchar(60),
			interior_color varchar(60),
			seller_rating float,
			owner_count float,
			trimId varchar(10),
			trim_name varchar(100),
			FOREIGN KEY (sp_id) REFERENCES Dealer(sp_id),
			index (body_type),
			index (price),
			index (owner_count)
		  );

load data infile '/tmp/debug/Car.csv' ignore into table Car
-- load data infile '/tmp/Car.csv' ignore into table Car
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
