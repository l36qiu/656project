SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS PickupTruck;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create PickupTruck' as '';

create table if not exists PickupTruck (
			Pickup_id int not null primary key,
			-- pickuptruck_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			-- vin varchar(22),
			bed varchar(7),
			bed_height varchar(2),
			bed_length varchar(7),
			cabin varchar(14),
			
			-- PRIMARY KEY (pickuptruck_id),
			FOREIGN KEY (Pickup_id) REFERENCES Car(Car_id)
		  );

load data infile '/tmp/debug/PickupTruck.csv' ignore into table PickupTruck
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
    (Pickup_id, @bed, @bed_height, @bed_length, @cabin)
	SET
	bed := if(@bed = "", NULL, @bed),
	bed_height := if(@bed_height = "", NULL, @bed_height),
	bed_length := if(@bed_length = "", NULL, @bed_length),
	cabin := if(@cabin = "", NULL, @cabin);