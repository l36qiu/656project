drop table if exists DepreciationInfo;
drop table if exists DynamicInfo;
drop table if exists FuelInfo;
drop table if exists PickupTruck;
drop table if exists SalesWebInfo;
drop table if exists SpaceInfo;
drop table if exists Car;
drop table if exists Dealer;