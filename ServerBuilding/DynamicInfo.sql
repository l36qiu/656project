SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS DynamicInfo;
SET FOREIGN_KEY_CHECKS = 1;

select '-----------------------------------------------------------------' as '';
select 'Create DynamicInfo' as '';

create table if not exists DynamicInfo (
			Dy_id int not null primary key,
			-- dynamicinfo_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
			-- vin varchar(22),
			engine_type varchar(25),
			engine_cylinders varchar(25),
			engine_displacement float,
			horsepower float,
			torque varchar(21),
			transmission varchar(11),
			transmission_display varchar(34),
			wheel_system varchar(21),
			wheel_system_display varchar(17),
			wheelbase varchar(8),
			power varchar(20),

			-- PRIMARY KEY (dynamicinfo_id),
			FOREIGN KEY (Dy_id) REFERENCES Car(Car_id)
		  );

load data infile '/tmp/debug/DynamicInfo.csv' ignore into table DynamicInfo
    fields terminated by ','
    enclosed by '"'
    lines terminated by '\n'
    ignore 1 lines 
    -- (   
	-- 	vin,
	-- 	engine_type,
	-- 	engine_cylinders,
	-- 	engine_displacement,
	-- 	horsepower,
	-- 	torque,
	-- 	transmission,
	-- 	transmission_display,
	-- 	wheel_system,
	-- 	wheel_system_display,
	-- 	wheelbase,
	-- 	power
	-- 	);